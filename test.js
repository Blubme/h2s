const {
  F,
  T,
  pipe,
  map,
  uniq,
  filter,
  uppercase
} = require("./dist/morphling.umd");
const tap = require("./source/functions/tap");

const shoplists = [
  { id: 1, name: "banana", type: "fruit" },
  { id: 2, name: "skate", type: "sport-tool" },
  { id: 3, name: "knife", type: "kitchen-tool" },
  { id: 3, name: "knife", type: "kitchen-tool" }
];

pipe(
  map("type"),
  tap(console.log),
  map(uppercase),
  tap(console.log),
  filter(x => x === "FRUIT"),
  tap(console.log),
  uniq
)(shoplists);
