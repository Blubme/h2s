# 🌊 H2S

tiny library it can disallow you using much on method.

> this is just a syntatic sugar of js methods , all wrapped to the **pure function**.

**“Sometimes, the elegant implementation is just a function. Not a method. Not a class. Not a framework. Just a function.” ~ John Carmack (Id Software, Oculus VR)”**

## Examples

### Map

```js

import { map } from 'h2s'

const shoplists = [
  {id: 1, name: "banana", type: "fruit"},
  {id: 2, name: "skate", type: "sport-tool"},
  {id: 3, name: "knife", type: "kitchen-tool"}
]

wanna take the name only ?

-> using traditional map

const nameLists = shoptlists.map(list => list.name)
console.log(nameLists) => # ["banana","skate","knife"]

-> using orb

const nameLists = map("name")(shoplists)
console.log(nameLists) => # ["banana","skate","knife"]
```

### Pipe

```js

// pipe is like going run multiple functions into a single flow (left to right)

import { pipe, map, uniq, uppercase } from 'h2s'

const shoplists = [
  {id: 1, name: "banana", type: "fruit"},
  {id: 2, name: "skate", type: "sport-tool"},
  {id: 3, name: "knife", type: "kitchen-tool"}
  {id: 3, name: "knife", type: "kitchen-tool"}
]

const newObject = pipe(
  map("name"),
  map(uppercase),
  uniq
)(shoplists)

console.log(newObject) => # ["BANANA","SKATE","KNIFE"]

```

### Even helping to create HTML tag ? yes, use { tag }

```js
import { pipe, map, join } from "h2s";
import { tag } from "h2s-dom";

const listGroup = tag({ tag: "ul", attr: { class: "list-group" } });
const listGroupItem = tag({ tag: "li", attr: { class: "list-group-item" } });
const listGroupItems = items => pipe(join(""))(map(listGroupItem)(items));

listGroup();
// <ul class="list-group"></ul>

listGroupItem("Cras justo");
// <li class="list-group-item">Cras justo</li>

listGroupItems(["Cras justo", "Dapibus ac"]);
// <li class='list-group-item'>Cras justo</li>
// <li class='list-group-item'>Dapibus ac</li>

listGroup(listGroupItems(["Cras justo", "Dapibus ac"]));
// <ul class='list-group'>
//   <li class='list-group-item'>Cras justo</li>
//   <li class='list-group-item'>Dapibus ac</li>
// </ul>
```

### Future HTML ELEMENT INJECT (WIP)

```js
// this is the proof concept, it will be more simplified soon

const button = tag({
  tag: "button",
  attr: { type: "button", class: "btn btn-danger", style: "margin-left: 10px;" }
}); // parent component
const button_content = tag({
  tag: "span",
  attr: { class: "badge badge-light" }
}); // children component

const new_elm = button(["Notification ", button_content("4")].join("")); // combine

let gambar =
  "https://images.unsplash.com/photo-1564668848852-713edc63f31c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=335&q=80";
const thumbnail = tag({ tag: "div", attr: { class: "thumbnail" } });
const thumb_img = tag({
  tag: "img",
  attr: { src: `${gambar}`, style: "height: 300px", alt: "sarkem_di_2040" }
});
const thumb_caption = tag({ tag: "div", attr: { class: "caption" } });
const t_h3 = tag({ tag: "h3", attr: {} });
const t_p = tag({ tag: "p", attr: {} });
const t_pa = tag({
  tag: "a",
  attr: { class: "btn btn-primary", role: "button" }
});

// combine all
const thumbnail_elm = thumbnail(
  [
    thumb_img(""),
    thumb_caption(
      [
        t_h3("Perjalanan mistis di sarkem"),
        t_p(
          "disini merupakan deskripsi dimana sarkem ditemukan oleh seorang pemuda bernama coco."
        ),
        t_pa("Aku Button"),
        new_elm
      ].join("")
    )
  ].join("")
);

document.getElementById("root").insertAdjacentHTML("afterend", thumbnail_elm);
```
